<cfcomponent>

	<cfset this.name = "GTOStaging">
	<cfset this.sessionmanagement = "true">
	<cfset this.sessiontimeout = createtimespan(0,2,0,0)>
	<cfset this.clientmanagement = "true">
	<cfset this.clientStorage = "primary_gto">
	<cfset this.setclientcookies = "true">
	<cfset this.loginstorage = "session">
	<cfset this.setDomainCookies = "yes">
	<cfset this.scriptProtect="none">
	<cfset this.ApplicationTimeout=createtimespan(0,1,0,0)>

	<!----cfset this.restsettings.cfclocation = "./Model,./API">
    <cfset this.restsettings.skipcfcwitherror = true---->

	<cfset this.mappings = structNew()>
	<cfset THIS.mappings["/PCHMap"]="c:\inetpub\wwwroot\puntersclub">
	<cfset THIS.mappings["/cfmongodb"]="c:\inetpub\wwwroot\cfmongodb">

	<cferror type="Request" template="/Admin/Error/GTOErrorPage.cfm" mailto="support@thegreattipoff.com">

	<cferror type="Exception" template="/Admin/Error/GTOErrorPage.cfm" mailto="support@thegreattipoff.com">

	<!--- general support function --->
	<cffunction name="findfile" access="private" returntype="string">
		<cfargument type="string" name="filename" required="yes" />

		<cfset var root = listfirst(expandpath('.'),"\") & "\">
		<cfset var path = "">
		<cfset var filespec = expandpath('.') & "\" & arguments.filename>

		<cfif not fileexists(filespec)>
			<cfloop condition="true">
				<cfset path = path & "..\">
				<cfset filespec = expandpath(path) & "\" & arguments.filename>
				<cfif fileExists(filespec)>
					<cfbreak>
				</cfif>
				<cfif path is root>
					<cfthrow message="File #arguments.filename# not found">
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn filespec>
	</cffunction>

<!---- ON APPLICATION START ----->

	<cffunction name="onApplicationStart" returntype="Boolean">

		<cfset var iniPath = findfile("Application.ini")>
		<cfset application.mongohosts = arrayNew(1)>
		<cfset application.basehref = trim(getProfileString(iniPath,"Global","basehref"))>
		<cfset application.datasource = trim(getProfileString(inipath,"Global","datasource"))>
		<cfset application.footydatasource = trim(getProfileString(inipath,"Global","footydatasource"))>
		<cfset application.second_datasource = trim(getProfileString(inipath,"Global","second_datasource"))>
		<cfset application.second_footydatasource = trim(getProfileString(inipath,"Global","second_footydatasource"))>
		<cfset application.mongoServerName = trim(getProfileString(inipath,"Global","mongo-servername"))>
		<cfset application.mongoServerPort = trim(getProfileString(inipath,"Global","mongo-serverPort"))>
		<cfset application.mongoDBName = trim(getProfileString(inipath,"Global","mongo-dbName"))>
		<cfset application.mongohosts[1] = structNew()>
		<cfset application.mongohosts[1].ServerName = #application.mongoServerName#>
		<cfset application.mongohosts[1].ServerPort = #application.mongoServerPort#>
		<cfset application.installdir = trim(getProfileString(inipath,"Global","installdir"))>
		<cfset application.serverdomain = trim(getProfileString(inipath,"Global","serverdomain"))>
		<cfset application.lAdministratorIPs = trim(getProfileString(inipath,"Global","lAdministratorIPs"))>
		<cfset application.cfcpath = trim(getProfileString(inipath,"Global","cfcpath"))>
		<cfset application.serverAlias = trim(getProfileString(inipath,"Global","serverAlias"))>
		<cfset application.basepath = left(getDirectoryFromPath(inipath),len(getDirectoryFromPath(inipath))-1) & "application\">
        <cfset application.LoginError = "Sorry Username or Password Incorrect!!">
		<cfset application.SiteName = "gto.racingtipoff.com">
		<cfset application.SiteLabel = "theGreatTipOff">

		<cfset application.paymentsite = "http://gto.racingtipoff.com">

		<!--- Replace turftips with thegreattipoff.com ---->
		<cfset application.ImagePath = "#application.basehref#/assets/images">
		<cfset application.ImageTempPath = "#application.basehref#/assets/images/imgwork">
		<cfset application.viewpath = "#application.basehref#/view">
		<cfset application.datapath = "#application.basehref#/data">
		<cfset application.enginepath = "#application.basehref#/engine">
		<cfset application.adminpath = "#application.basehref#/admin">
		<cfset application.APIpath = "#application.basehref#/API">
		<cfset application.stylepath = "#application.basehref#/assets/stylesheets">
		<cfset application.funcpath = "#application.basehref#/assets/scripts">
		<cfset application.scriptpath = "#application.basehref#/assets/scripts/js">
		<cfset application.jquerypath = "#application.basehref#/assets/scripts/jquery">
		<cfset application.modelpath = "">
		<cfset application.GTOmodelpath = "">
		<cfset application.cfcpath = "#application.basehref#/model">
		<cfset application.PCHmodelpath = "PCHMap.">
		<cfset application.rootpath = "">
		<cfset application.homepath = "#application.sitename#/index.cfm">
		<cfset application.usersitepath = "#application.basehref#/usersite">
		<cfset application.usersitepathtemplate = "#application.basehref#/usersite/sitetemplates">
		<cfset application.defaultsiteimagepath = "#application.basehref#/assets/Images/UserSiteImage">
		<cfset application.defaultsitepath = "#application.basehref#/assets/Images/UserSiteImage/SiteTemplate">
		<cfset application.AvatarPath = "#application.basehref#/assets/images/avatar">
		<cfset application.affiliatepath = "#application.basehref#/view/affiliate">
		<cfset application.silkpath = "https://www.bookmaker.com.au">


		<cfset application.usiteendurl = "#application.sitename#">
		<cfset application.defaultCompWebpath = "#application.SiteName#/racecomp/">
		<cfset application.embedsitepath = "#application.basehref#/site/embed">
		<cfset application.siteteam = "Great Tip Off Team">
		<cfset application.maillabel = "Great Tip off">

		<cfset application.mailserver = "mail.thegreattipoff.com">
		<!---cfset application.outgoingmailserver = "127.0.0.1"---->
		<cfset application.outgoingmailserver = "smtp.mailgun.org">
		<cfset application.mailusername = "postmaster@mailing.thegreattipoff.com">
		<cfset application.mailuserpassword = "77003102487e8fb077a065255ce7a160">

		<cfset application.mailAPIKey ="key-a80bc56c693dd7adaaf72a4358e90ac5">
		<cfset application.mailBaseUrl ="https://api.mailgun.net/v3/mailing.thegreattipoff.com">

		<cfset application.elasticmailurl = "https://api.elasticemail.com/v2/email/send">
		<cfset application.myelasticuser = "spowell@thegreattipoff.com" >
		<cfset application.myelasticAPIKey = "76c42414-dfaa-4996-9a76-355d7a80db8a" >
		
		<cfset application.Partnermailusername = "postmaster@mailing.tipcompmail.com">
		<cfset application.Partnermailuserpassword = "bff8173f34ccb02b4a10f0db7dda2af6">

		<cfset application.mailreguser = "customerservice@thegreattipoff.com">
		<cfset application.mailsupuser = "support@thegreattipoff.com">
		<cfset application.mailCustuser = "customerservice@thegreattipoff.com">
		<cfset application.mailMarketuser = "marketing@thegreattipoff.com">
		<cfset application.mailCompadmin = "compadmin@thegreattipoff.com">
		<cfset application.mailTipsterTalk = "tipstertalk@thegreattipoff.com">
		<cfset application.mailAbuse = "contentabuse@thegreattipoff.com">
		<cfset application.mailTipAlerts = "tipalerts@thegreattipoff.com">
		<cfset application.mailsales = "tipsales@thegreattipoff.com">
		<cfset application.mailPPadmin = "PPAdmin@thegreattipoff.com">
		<cfset application.mailpass = "registerGTO2009">

		<cfset application.walletminimum = 10>
		<cfset application.tipchargemin = 10>
		<cfset application.packagepricemin = 7>
		<cfset application.bookiebetmin = 0.5>
		<cfset application.betpoolpc = 0.05>
		<cfset application.minbet = 10>
		<cfset application.smscharge = 0.22>
		<cfset application.packagetimes = 3>  <!--- Number of time pacakge price for back bet 4 ----->

		<cfset application.reportpath = "C:\Users\Administrator\Documents\WebsiteReporting\">
		<cfset application.mailAdmin = "spowell@thegreattipoff.com; support@thegreattipoff.com">

		<cfset application.merchant = "3E17CCCCF5CB9BDA7B96835F1E4EE8DA">
		<cfset application.merchantpass = "13A39C0FF683A4C5F7185D6605C56A25">

		<cfset application.eusername = "60CF3AD1fNPvPe5wXTlvhiYuGRaZhj7LKDNu9nC8ktsDfPNOsSWECz+f2t8TdRxKAk+5CQ">
		<cfset application.epassword = "A6281374FBF6AADFA0D307EB869C5063">
		<cfset application.eCustID = "18351611">
		<cfset application.eMyID = "spowell@thegreattipoff.com">

		<!----cfset application.livemerchantpass = "13A39C0FF683A4C5F7185D6605C56A25">
		<cfset application.testmerchantpass = "9081B50A22C7AF1C5754CCA0313E8146"---->

		<cfset application.seckey = "45ePx+hbX2daQKnI2Hg1aA==">
		<cfset application.siteseckey = "V7ek35N7hUZMG7pvd3xy9w== ">

		<cfset application.minrankTips = 1>

		<cfset application.constipstarget = 65>
		<cfset application.winprofittarget = 25>

		<!----cftry>
				<cfset application.mongo = createObject('component','#application.modelpath#Model.mongo').init(dbName="mongogto")>
			<cfcatch>
			</cfcatch>
		</cftry----->
        <!----cftry>
          <!--- Test whether the DB that this application uses is accessible
                by selecting some data. --->
          <cfquery name="testDB" dataSource="#application.datasource#" maxrows="2">
             SELECT userid FROM user
          </cfquery>
          <!--- If we get database error, report it to the user, log the error
             information, and do not start the application. --->
          <cfcatch type="database">
             <cfoutput>
                This application encountered an error.<br>
                Please contact support.
             </cfoutput>
             <cflog file="#This.Name#" type="error"
                text="cfdocexamples DB not available. message: #cfcatch.message#
                Detail: #cfcatch.detail# Native Error: #cfcatch.NativeErrorCode#">
             <cfreturn False>
          </cfcatch>
       </cftry---->

		<cfset application.maintenancemode = "false">

		<cflog text="Application Startup" type="Information" file="#this.name#">

		<cfif not isdefined("application.activesessions")>
			<cfset application.activesessions = 0>
		</cfif>

		<cfreturn "true">

	</cffunction>

<!---- ON APPLICATION END ----->

	<cffunction name="onApplicationEnd" returntype="Void">
		<cfargument name="ApplicationScope" required="True" />

		<cflog text="Application Shutdown" type="Information" file="#this.name#">

	</cffunction>

<!---- ON SESSION START ----->

    <cffunction name="onSessionStart" output="false">
      <cfscript>
         session.started = now();
      </cfscript>

      <cfset session.isloggedin = false>
      <cfset session.LoginError = "">
	  <cfset session.LoginMobile = "N">

	  <cfif #findnocase('bot', cgi.HTTP_USER_AGENT)# eq 0>

		      <cflock scope="application" timeout="25">
		         <cfset application.activesessions = application.activesessions + 1>
		      </cflock>

			 <cflog file="#this.name#" type="Information"
		            text="Session #session.sessionid# Started. Length: #session.started# Active sessions: #application.activesessions#">
	  </cfif>

   </cffunction>

<!---- ON SESSION END ----->

   <cffunction name="onSessionEnd" output="false">
       <cfargument name = "sessionScope" required=true/>
       <cfargument name = "applicationScope" required=true/>
       <cfset var sessionLength = TimeFormat(Now() - sessionScope.started, "H:mm:ss")>

       <cflock name="AppLock" timeout="25" type="Exclusive">
            <cfset arguments.applicationScope.activesessions = arguments.applicationScope.activesessions - 1>
       </cflock>

       <cflog file="#this.name#" type="Information"
            text="Session #arguments.sessionScope.sessionid# ended. Length: #sessionLength# Active sessions: #arguments.applicationScope.activesessions#">

		<!----cfif isdefined("cookie.cfid") and isdefined("cookie.cftoken")>
			<cfset cfid_local = cookie.cfid>
			<cfset cftoken_local = cookie.cftoken>
			<cfcookie name="cfid" value="#cfid_local#" expires="NOW">
			<cfcookie name="cftoken" value="#cftoken_local#" expires="NOW">
			<cfloop collection="#cookie#" item="c">
					<cfset c = structDelete(cookie,c)>
			</cfloop>
		</cfif>

		<cfcookie name="cfid" value="#cfid#" expires="NOW">
		<cfcookie name="cftoken" value="#cftoken#" expires="NOW">

		<cfscript>
			   StructClear(Session);
		</cfscript>

		<cfset StructDelete(Session, "cftoken")>
 		<cfset StructDelete(Session, "cfid")---->

	</cffunction>

<!---- ON REQUEST START ---->

	<cffunction name="onRequestStart" returntype="Boolean">
		<cfargument name="targetpage" type="string" required="true" />
		<cfset request.notssl = true>
		<!--- Set Header Code
				<cfheader statuscode="503" statustext="Service Temporarily Unavailable" />
				<!--- Set retry time --->
				<cfheader name="retry-after" value="3600" />
				<cfinclude template="/GTOMaintenance.html">
				<cfabort>--->
		<cfif #cgi.server_port# neq 81>
				<!----cfif StructKeyExists(GetHttpRequestData().headers, "X-CLIENT-IP") >
					<cfset REQUEST.remoteAddress = Trim(ListFirst(GetHttpRequestData().headers["X-CLIENT-IP"])) >
				<cfelseif  StructKeyExists(GetHttpRequestData().headers, "X-Forwarded-For") >
					<cfset REQUEST.remoteAddress = Trim(ListFirst(GetHttpRequestData().headers["X-Forwarded-For"])) >
					<cfset request.notssl = true>
					<cfif #findnocase("index.cfm", cgi.SCRIPT_NAME)# gt 0 OR
					(#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("register", cgi.query_string)# gt 0) OR
					(#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("bettingaccounts", cgi.query_string)# gt 0)>

								<cfif #findnocase("index.cfm", cgi.SCRIPT_NAME)# gt 0>
									<cflocation url="https://#cgi.SERVER_NAME#" addtoken="false">
								<cfelseif (#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("register", cgi.query_string)# gt 0)>
									<cflocation url="https://#cgi.SERVER_NAME#/join" addtoken="false">
								<cfelseif (#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("bettingaccounts", cgi.query_string)# gt 0)>
									<cflocation url="https://#cgi.SERVER_NAME#/user/bettingaccounts" addtoken="false">
								<!----cfelseif #findnocase("login.cfm", cgi.SCRIPT_NAME)# gt 0>
									<cflocation url="https://#cgi.SERVER_NAME#/login" addtoken="false">
								<cfelseif (#findnocase("startclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #len(cgi.query_string)# eq 0)>
									<cflocation url="https://#cgi.SERVER_NAME#/startclub" addtoken="false">
								<cfelseif (#findnocase("joinclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #len(cgi.query_string)# gt 0)>
									<cflocation url="https://#cgi.SERVER_NAME#/joinclub?#cgi.query_string#" addtoken="false">
								<cfelseif (#findnocase("startclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("start-step1", cgi.query_string)# gt 0)>
									<cflocation url="https://#cgi.SERVER_NAME#/startclub/start-step1" addtoken="false">
								<cfelseif #findnocase("contact", cgi.query_string)# gt 0>
									<cflocation url="https://#cgi.SERVER_NAME#/contact" addtoken="false"---->
								</cfif>
					</cfif>
				<cfelse>
					<cfset REQUEST.remoteAddress = #cgi.remote_addr# >
				</cfif---->
				<cfset REQUEST.remoteAddress = #cgi.remote_addr# >
			<cfelse>
					<cfset REQUEST.remoteAddress = #cgi.remote_addr# >
			</cfif>

		<!----cfif #findnocase('58.108.229.233',REQUEST.remoteAddress)#>
			<cfdump var="#cgi#"><cfabort>
		</cfif---->
<!----cfoutput>Hotbet is currently down for routine updates, we will be back shortly</cfoutput><cfabort------>
    	<cfset application.activeusers = 100>

		<!----cfif #findnocase('121.217.247.248', REQUEST.remoteAddress)# eq 0 and #findnocase('118.127.18.', REQUEST.remoteAddress)# eq 0>

				<!--- Set Header Code --->
				<cfheader statuscode="503" statustext="Service Temporarily Unavailable" />

				<!--- Set retry time --->
				<cfheader name="retry-after" value="3600" />

				<!----cfinclude template="#application.adminpath#/error/GTOMaintenancePage.cfm"----->

				<!----cfif structKeyExists(application, "imagepath")>
					<p><cfoutput><img src="#application.ImagePath#/logo.jpg"></cfoutput></p>
				</cfif---->

				 <h1>Down for Upgrade</h1>

				 <p> The Great Tip Off website is currently down for an upgrade and will be back up shortly.
					 Sorry for the inconvenience.</p>
		    	<p><strong>The Great Tip Off Support</strong></p>

				<cfreturn "false">
		</cfif---->

		<cfif isdefined("url.init")>
			<cfset onApplicationStart()>
		</cfif>

		<!-----cftry>
          <!--- Test whether the DB that this application uses is accessible
                by selecting some data. --->
          <cfquery name="testDB" dataSource="#application.datasource#" maxrows="1">
             	SELECT userid FROM user
          </cfquery>
          <!--- If we get database error, report it to the user, log the error
             information, and do not start the application. --->
          <cfcatch type="any">
             <cfmail
				to="bas242@gmail.com;support@thegreattipoff.com;" from="errorsender@thegreattipoff.com"
				subject="#application.sitename# - Primary Database Down!!">
						 Website: #application.sitename#
						 Server: #application.serverAlias# - #application.lAdministratorIPs#
						 Please check the Primary database server!

						 DB not available. message: #cfcatch.message#

						 Detail: #cfcatch.detail#

						 Native Error: #cfcatch.NativeErrorCode#
				</cfmail>

				<cfset application.datasource = trim(getProfileString(inipath,"Global","second_datasource"))>
			    <cfset application.footydatasource = trim(getProfileString(inipath,"Global","second_footydatasource"))>
		        <cfset this.clientStorage = #application.datasource#>

	            <cfreturn true>
          </cfcatch>
        </cftry----->

		<cfparam name="session.isloggedin" default="false">

		 <cfif IsDefined("Form.logout")>
		      <cflogout>
		 </cfif>

		 <!---- Process Login AND (findnocase(form.loginid, 'basman') gt 0 OR findnocase(form.loginid, 'ladyluck') gt 0 OR findnocase(form.loginid, 'damostar') gt 0 OR findnocase(REQUEST.remoteAddress, '121.217.247.248') gt 0 OR findnocase(REQUEST.remoteAddress, '58.111.147.151') gt 0 ) ---->
		 <cfif #session.isloggedin# eq false>
			 <cfif #structKeyExists(form,"loginid")# AND #structKeyExists(form,"password")#  AND #structKeyExists(form,"sitelogin")# >
			 		 <!--- log user into GTO ---->
				  	 <cflock  timeout="25" scope="session">
							<cfset request.slogin = createObject("component", "#application.modelpath#Model.login").GTOlogin(formval=#form#)>
					 </cflock>

					 <cfif #session.isloggedin# eq true>
					 		<cfthread name="checkBets#session.userid#" action="run" priority="high"
										myuser="#session.userid#">

									<cfquery name="qClubs" datasource="#application.datasource#">
											SELECT * FROM clubmaster where clubtype = 'P' AND manager = <cfqueryparam value="#myuser#" cfsqltype="cf_sql_integer">
									</cfquery>
									<cfloop query="qClubs">
											<cflock  timeout="20" scope="session">
													<cfset var tempbet = createObject("component", "#application.modelpath#model.cgi").BMgetMyBetsControl(updateonly=0, clubid=#qClubs.clubid# )>
											</cflock>
									</cfloop>
							</cfthread>

							<cfif #structKeyExists(url,"home")# eq true>
									<cflocation url="http://#application.sitename#/racing" addtoken="false">
							</cfif>
					 </cfif>
			 </cfif> <!---- END check if form and password submitted ----->
		 <cfelse>
				<cfif #structKeyExists(session,"sessionid")# >
					<cfif #structKeyExists(session,"userid")#>
			 			 <cflock  timeout="25" scope="session">
								<cfset testSession = createObject("component", "#application.modelpath#Model.login").checksession(userid=#session.userid#, sid=#session.sessionID#)>
						 </cflock>


					<!-----cfdump var="#testSession#"><cfabort------>
						 <cfif #testSession# eq true>
							 	<cfscript>
								   StructClear(Session);
								</cfscript>

								<cfcookie name="CFID" value="" expires="NOW" />
								<cfcookie name="CFTOKEN" value="" expires="NOW" />
								<cfcookie name="JSESSIONID" value="" expires="NOW" />

								<cfset session.isloggedin = false>
					            <cflocation url="http://#application.sitename#" addtoken="false">
						 </cfif>
					 </cfif>
				 </cfif>

		 </cfif> <!----- check if session is logged in ----->


		<cfreturn "true">
	</cffunction>

<!--- ON REQUEST START - MAINTENANCE

<cffunction name="onRequestStart" returntype="Boolean" output="true">
		<cfargument name="targetpage" type="string" required="true" />
<cfdump var="here on 92"><cfabort>
	   <cfset var LOCAL = structNew() />

	   <cfif (#REQUEST.remoteAddress# neq "101.164.209.12" AND #REQUEST.remoteAddress# neq "211.30.210.91" AND #REQUEST.remoteAddress# neq "127.0.0.1") and
	   		  #findnocase('API', CGI.SCRIPT_NAME)# eq 0>


				<!--- Set Header Code --->
				<cfheader statuscode="503" statustext="Service Temporarily Unavailable" />

				<!--- Set retry time --->
				<cfheader name="retry-after" value="3600" />

				<cfinclude template="#application.adminpath#/error/GTOMaintenancePage.cfm">

				<!----cfif structKeyExists(application, "imagepath")>
					<p><cfoutput><img src="#application.ImagePath#/logo.jpg"></cfoutput></p>
				</cfif>

				 <h1>Down for Maintenance</h1>

				 <p> The Great Tip Off website is currently down for maintenance and will be back up shortly.  Sorry for the inconvenience.</p>
		    	<p><strong>The Great Tip Off Support</strong></p--->

				<cfreturn "false">

		<cfelse>
				<cfset application.activeusers = 100>

				<cfif isdefined("url.init")>
					<cfset onApplicationStart()>
				</cfif>

				 <cfif IsDefined("Form.logout")>
				      <cflogout>
				  </cfif>

				 <cfreturn "true">
		</cfif>
	</cffunction> ---->

<!---- ON REQUEST ---->

	<cffunction name="onRequest"  returntype="boolean">
		<cfargument name="targetpage" type="string" required="true" />

		<cfif #findnocase('Googlebot', cgi.HTTP_USER_AGENT)# eq 0 AND
				#findnocase('bingbot', cgi.HTTP_USER_AGENT)# eq 0 AND
					#findnocase('yahoo', cgi.HTTP_USER_AGENT)# eq 0 AND
						#findnocase('msnbot', cgi.HTTP_USER_AGENT)# eq 0>
				<cfset limiter()>
		</cfif>

		<cfif #findnocase('193.138.219.228',REQUEST.remoteAddress)# OR
				#findnocase('199.241.145.219',REQUEST.remoteAddress)# OR
					#findnocase('185.133.32.19',REQUEST.remoteAddress)# OR
						#findnocase('80.78.250.21',REQUEST.remoteAddress)# OR
						#findnocase('5.39.216.134',REQUEST.remoteAddress)#
						>
			<cfabort>
		</cfif>

		<cfif #findnocase('46.229.168.69', REQUEST.remoteAddress)# gt 0
				AND #findnocase('46.229.168.71', REQUEST.remoteAddress)# gt 0
				AND #findnocase('SemrushBot', cgi.HTTP_USER_AGENT)# gt 0
				AND #findnocase('MJ12bot', cgi.HTTP_USER_AGENT)# gt 0
				AND #findnocase('AhrefsBot', cgi.HTTP_USER_AGENT)# gt 0>
			<cfabort>
		</cfif>

		<cfif #findnocase('103.2.196.105',cgi.SERVER_NAME)#>
				<cflocation url="http://#application.sitename#" addtoken="false">
		</cfif>

		<!----cfif #cgi.server_port# eq 443>
			<cfset request.protocol = 'https://'>
		<cfelse>
			<cfset request.protocol = 'http://'>
		</cfif---->

		<cfif #cgi.server_port# neq 81>
			<cfif StructKeyExists(GetHttpRequestData().headers, "X-CLIENT-IP") >
				<cfset request.protocol = 'http://'>
			<cfelse>
				<cfset request.protocol = 'http://'>
			</cfif>
		<cfelse>
			<cfset request.protocol = 'http://'>
		</cfif>

		<!---cfif #cgi.server_port# neq 443>
			<cflocation url="https://#application.sitename#" addtoken="false">
		</cfif---->

		<!---- SSL conditions
		<cfif #cgi.server_port# eq 443 AND #findnocase('action=bettingaccounts',cgi.QUERY_STRING)#>
				<cflocation url="http://#application.sitename#/user/bettingaccounts" addtoken="false">
		</cfif>

		<cfif #cgi.server_port# neq 443 AND #findnocase('action=contact',cgi.QUERY_STRING)#>
				<cflocation url="https://#application.sitename#/info/Contact" addtoken="false">
		</cfif>---->

		<!---cfif #findnocase('101.161.163.171', REQUEST.remoteAddress)#---->
			<!----cfif #findnocase("index.cfm", cgi.SCRIPT_NAME)# gt 0 OR
			  (#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("register", cgi.query_string)# gt 0) >
				<cfif #cgi.server_port# neq 443>
						<cfif #findnocase("index.cfm", cgi.SCRIPT_NAME)# gt 0>
							<cflocation url="https://#cgi.SERVER_NAME#" addtoken="false">
						<cfelseif (#findnocase("user.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("register", cgi.query_string)# gt 0)>
							<cflocation url="https://#cgi.SERVER_NAME#/join" addtoken="false">
						<!----cfelseif #findnocase("login.cfm", cgi.SCRIPT_NAME)# gt 0>
							<cflocation url="https://#cgi.SERVER_NAME#/login" addtoken="false">
						<cfelseif (#findnocase("startclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #len(cgi.query_string)# eq 0)>
							<cflocation url="https://#cgi.SERVER_NAME#/startclub" addtoken="false">
						<cfelseif (#findnocase("joinclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #len(cgi.query_string)# gt 0)>
							<cflocation url="https://#cgi.SERVER_NAME#/joinclub?#cgi.query_string#" addtoken="false">
						<cfelseif (#findnocase("startclub.cfm", cgi.SCRIPT_NAME)# gt 0 AND #findnocase("start-step1", cgi.query_string)# gt 0)>
							<cflocation url="https://#cgi.SERVER_NAME#/startclub/start-step1" addtoken="false">
						<cfelseif #findnocase("contact", cgi.query_string)# gt 0>
							<cflocation url="https://#cgi.SERVER_NAME#/contact" addtoken="false"---->
						</cfif>
				</cfif>
			</cfif---->
		<!----/cfif---->
		<!---- END SSL conditions ---->

		<cfset request.servertime = #now()#>
		<cfset request.nowtime = #now()#>


		<cfif not isdefined("application.sitesource")>
				<cfset application.sitesource = "GTO">
		</cfif>
		<cfif not isdefined("application.SourceScreen")>
				<cfset application.SourceScreen = "GTO">
		</cfif>
		<cfif not isdefined("application.bookmakerafilliatecode")>
				<cfset application.bookmakerafilliatecode = "19405">
		</cfif>
		<cfif not isdefined("application.bookmakerbonus")>
				<cfset application.bookmakerbonus = "GTO400">
		</cfif>
		<cfif not isdefined("application.bookieid")>
				<cfset application.bookieid = 2>
		</cfif>
		<cfif not isdefined("application.walletminimum")>
				<cfset application.walletminimum = 10>
		</cfif>
		<cfif not isdefined("application.tipchargemin")>
				<cfset application.tipchargemin = 10> <!--- go live change --->
		</cfif>

		<cfif not isdefined("application.bookiebetmin")>
				<cfset application.bookiebetmin = 0.5>
		</cfif>
		<cfif not isdefined("application.betpoolpc")>
				<cfset application.betpoolpc = 0.05> <!--- go live change --->
		</cfif>
		<cfif not isdefined("application.packagebetmin")>
				<cfset application.packagebetmin = 0.5>  <!--- Need to update to 4 ----->
		</cfif>
		<cfif not isdefined("application.packagetimes")>
				<cfset application.packagetimes = 3>  <!--- Number of time pacakge price for back bet 4 ----->
		</cfif>
		<cfif not isdefined("application.packagepricemin")>
				<cfset application.packagepricemin = 7>
		</cfif>
		<cfif not isdefined("application.minbet")>
				<cfset application.minbet = 10>  <!--- go live change --->
		</cfif>
		<cfif not isdefined("application.smscharge")>
				<cfset application.smscharge = 0.22>
		</cfif>

		<cfparam name="session.affLogin" default="false"/>
		<cfparam name="session.betauth" default="1" type="string">
		<cfparam name="session.ISBOOKIELOGGEDIN" default="false" type="boolean">
		<cfparam name="session.bookieid" default="-1" type="string">
		<cfparam name="session.betauth" default="1" type="string">
		<cfparam name="application.bookieid" default="2" type="string">
		<cfparam name="url.nexttojump" default="0">
		<cfparam name="session.racetypemode" default="R">
		<cfparam name="request.formguidemode" default="0">

		<!-----cfif #structKeyExists(session,"ipaddressvalid")# eq false>
				<cfhttp result="myResult" url="http://api.ipinfodb.com/v3/ip-country/?key=ffd5b0b38da76545177165da6c41ed4a2fb107d1bb94b29c1e6576befb935a12&ip=#REQUEST.remoteAddress#" method="get">
				<cftry>
					<cfif #listgetat(myResult.filecontent,3,";")# neq 'AU' AND #listgetat(myResult.filecontent,3,";")# neq 'NZ'>
							<cflock  timeout="20" scope="session">
								<cfset session.ipaddressvalid = false>
							</cflock>
					<cfelse>
							<cflock  timeout="20" scope="session">
								<cfset session.ipaddressvalid = true>
							</cflock>
					</cfif>
				<cfcatch>
						<cflock  timeout="20" scope="session">
								<cfset session.ipaddressvalid = false>
						</cflock>
				</cfcatch>
				</cftry>
		</cfif----->

		<cfset session.ipaddressvalid = true>

		<cfif #structKeyExists(application,"qAvailableBookies")# eq false>
				<!---- Get Available Bookies ---->
			 	<cflock scope="session" timeout="10">
						<cfset application.qAvailableBookies = createObject("component", "#application.modelpath#API.bookmaker").getAvailableBookies(sitesource = #application.sitesource#)>
				</cflock>
		</cfif>

		<cfif #structKeyExists(application,"qSitebookie")# eq false>
				 <cflock  timeout="20" scope="session">
						<cfset application.qSitebookie = createObject("component", "#application.modelpath#Model.info").getBookie(sitesource=#application.sitesource#)>
				 </cflock>
		</cfif>

		<cfif #session.afflogin# eq true>
			<cfif #structKeyExists(session,"safflogin")#>
				<cfset session.orgid = #session.safflogin.orgid#>
			</cfif>
		</cfif>

		<cfif #structKeyExists(application,"qRankAll")# eq false>
				<cflock scope="application" type="exclusive" timeout="25">
						<cfset temp = createObject("component", "#application.modelpath#Model.leaderboard").getLeaderboard()>
				</cflock>
		</cfif>

		<cfif #structKeyExists(application,"qCommission")# eq false>
				<cflock scope="application" type="exclusive" timeout="25">
						<cfset application.qCommission = createObject("component", "#application.modelpath#Model.tipsales").getPricing()>
				</cflock>
		</cfif>

		<cfif #structKeyExists(application,"qMemberfees")# eq false>
				<cflock scope="session" type="exclusive" timeout="25">
						<cfset application.qMemberfees = createObject("component", "#application.modelpath#Model.membership").memberfees()>
				</cflock>
		</cfif>

		<cfif #session.isloggedin# eq true>

				 <!----cflock  timeout="20" scope="session">
						<cfset session.bookieid = createObject("component", "#application.modelpath#Model.Login").CheckBookieLogin(userid=#session.userid#, sitesource = #application.sitesource#)>
				 </cflock---->

				 <cfif #session.bookieid# neq 0>
					 <cfset session.ISBOOKIELOGGEDIN = true>
				 </cfif>

				 <cfparam name="session.ISBOOKIELOGGEDIN" default="false" type="boolean">

				<!---- Get Wallet Balance ---->
			 	<cflock scope="session" timeout="20">
						<cfset request.GTOWallet = createObject("component", "#application.modelpath#Model.Wallet").getWalletBalance(userid=#session.userid#)>
				</cflock>

				<!----cfif #findnocase('101.191.28.210', cgi.remote_addr)#>
						<Cfdump var="#request.GTOWallet#"><Cfabort>
				</cfif---->

				<cflock scope="session" timeout="20">
						<cfset request.qAuthBookies = createObject("component", "#application.modelpath#API.bookmaker").getAvailableBookies(userid=#session.userid#, sitesource = #application.sitesource#, authorised=true)>
				</cflock>

				<!--- Get Follows ---->
				<cflock scope="session" timeout="20">
						<cfset request.sFollows = createObject("component", "#application.modelpath#Model.Info").getFollows(userid=#session.userid#)>
				</cflock>
				<!---cfdump var="#request.sFollows#"---->
				<!----cfdump var="#request.GTOWallet#"---->
				<!----cfdump var="#request.qPendingBetSlip#"><cfabort---->
			<!--- Check if need to link bookmaker account id's
				<cfif #request.qAuthBookies.recordcount# gt 0>

					<cflock scope="session" timeout="20">
							<cfquery name="request.qCheckEmptyBookies" dbtype="query">
								SELECT * FROM application.qAvailableBookies WHERE bookieid in(#valuelist(request.qAuthBookies.bookieid)#)
							</cfquery>
					</cflock>

					<cfif #request.qCheckEmptyBookies.recordcount# neq #application.qAvailableBookies.recordcount# AND
							#findnocase('bettingaccounts', cgi.QUERY_STRING)# eq 0 AND
								#findnocase('login', cgi.script_name)# eq 0>
					  		<cflocation url="http://#application.sitename#/user/bettingaccounts" addtoken="false" />
					</cfif>
				</cfif>----->
		<cfelse>
				<cfparam name="request.GTOWallet" default="0">
		</cfif>


		<cfif #structKeyExists(application,"memberperiod")# eq false>
				<cfset application.memberperiod = 'W'>
		</cfif>

		<cfif #structKeyExists(url,"racetype")#>
				<cfset url.racetype = #UCASE(MID(url.racetype,1,1))#>
				<cfif #url.racetype# eq "F">
					<cfset url.racetype= 'R'>
					<cfset request.formguidemode = 1>
				</cfif>
		</cfif>
		<cfparam name="url.racetype" default="R">

		<cfif #structKeyExists(session, "racestartdate")# eq false>
				<cfif hour(now()) gte 0 AND hour(now()) lt 5>
						<cfparam name="session.racestartdate" default="#dateformat(dateadd('d',-1,now()),'yyyy-mm-dd')#">
						<cfparam name="session.raceenddate" default="#dateformat(dateadd('d',-1,now()),'yyyy-mm-dd')#">
				<cfelse>
						<cfparam name="session.racestartdate" default="#dateformat(now(), 'yyyy-mm-dd')#">
						<cfparam name="session.raceenddate" default="#dateformat(now(), 'yyyy-mm-dd')#">
				</cfif>
		</cfif>

		<cflock  timeout="20" scope="session">
				<cfset request.qRaceDates = createObject("component", "#application.modelpath#Model.selecttips").getracedates(racetype=#url.racetype#)>
		</cflock>

		<cfif #structKeyExists(session,"userid")# AND #session.isloggedin# eq true>
				<cfif #session.userid# gt 0>
					<cflock scope="session" type="exclusive" timeout="25">
							<cfset session.sMemberStatus = createObject("component", "#application.modelpath#Model.membership").memberstatus(userid=#session.userid#)>
					</cflock>

					<cflock scope="session" type="exclusive" timeout="25">
							<cfset request.sMemberStatus = createObject("component", "#application.modelpath#Model.membership").memberstatus(userid=#session.userid#, rounddate=#now()#)>
					</cflock>

					<cfif #request.sMemberStatus.validmember# eq true>
							<!--- get the active product ---->
							<cflock scope="session" type="exclusive" timeout="25">
									<cfset request.qProduct = createObject("component", "#application.modelpath#Model.product").getProduct(catid=2, active=true, sitesource=#application.sitesource#)>
							</cflock>
					</cfif>

					<cflock scope="session" timeout="10">
							<cfquery name="request.qMyBookies" dataSource="#application.datasource#">
								SELECT b.bookieid, b.defaultbookie, b.betacctid, b.userid, o.apiprotocol, o.apipath FROM bookiemapping b, bookie o
								WHERE b.userid = <cfqueryparam value="#session.userid#" cfsqltype="cf_sql_integer"> AND
									  b.bookieid = o.bookieid
							</cfquery>

							<cfquery name="request.qDefaultBookie" dbtype="query" maxrows="1">
								SELECT bookieid, apiprotocol, apipath, betacctid FROM request.qMyBookies
								WHERe defaultbookie != 0
							</cfquery>

							<cfquery name="request.qCheckGTOBookie" datasource="#application.datasource#">
									SELECT c.bookieid
									FROM clubmaster c
									WHERE c.manager =<cfqueryparam value="#session.userid#" cfsqltype="cf_sql_integer"> AND
										  c.clubtype = 'P'
							</cfquery>

							<cfif #request.qDefaultBookie.recordcount# gt 0>
									<cfif #session.defaultbookie# neq #request.qDefaultBookie.bookieid#>
										<cfset session.defaultbookie = #request.qDefaultBookie.bookieid#>
									</cfif>
									<cflock scope="session" type="exclusive" timeout="25">
											<cfset request.qVerify = createObject("component", "#application.modelpath#Model.cgi").BMVerify(accountid=#request.qDefaultBookie.betacctid#, userid=#session.userid#, bookieid=#request.qDefaultBookie.bookieid#)>
									</cflock>
									<!----cfif #session.userid# eq 1>
											<cfdump var="#request.qVerify#">
									</cfif---->

							<cfelse>
									<cfset session.defaultbookie = 0>
							</cfif>
					</cflock>

					<!---- Check if GTO bookmaker account been created, if not then clone
					<cfloop query="request.qMyBookies">
							<cfquery name="request.qCheckGTOBookie" datasource="#application.datasource#">
									SELECT c.clubid
									FROM clubmaster c
									WHERE c.manager =<cfqueryparam value="#session.userid#" cfsqltype="cf_sql_integer"> AND
										  c.clubtype = 'P' AND
										  c.bookieid = <cfqueryparam value="#request.qMyBookies.bookieid#" cfsqltype="cf_sql_integer">
							</cfquery>

							<cfif #request.qCheckGTOBookie.recordcount# eq 0>
									<cflock scope="session" timeout="10">
											<cfset request.sMemberStatus = createObject("component", "#application.modelpath#API.Bookmaker").GTOcreateBMAccount(userid=#session.userid#, bookieid=#request.qMyBookies.bookieid#)>
									</cflock>
									<cfdump var="#request.sMemberStatus#">
									<cfset thread = CreateObject("java", "java.lang.Thread")>
									<cfset thread.sleep(2000)>
									<Cfabort>
							</cfif>
					</cfloop>
					----->

				</cfif>
		</cfif>

		<cfif structKeyExists(session, "isloggedin") eq true>
				<cfinclude template="#arguments.targetpage#">
		<cfelse>
				<cfinclude template="#Arguments.targetPage#">
		</cfif>

		<cfreturn "true">
	</cffunction>

	<cffunction name="limiter">
	   <!---
	      - Throttles requests made more than "count" times within "duration" seconds from single IP.
	      - sends 503 status code for bots to consider as well as text for humans to read
	   --->

	   <cfargument name="count" type="numeric" default="7">
	   <cfargument name="duration" type="numeric" default="7">

	   <cfif not IsDefined("application.rate_limiter")>
	      <cfset application.rate_limiter = StructNew()>
	      <cfset application.rate_limiter[REQUEST.remoteAddress] = StructNew()>
	      <cfset application.rate_limiter[REQUEST.remoteAddress].attempts = 1>
	      <cfset application.rate_limiter[REQUEST.remoteAddress].last_attempt = Now()>
	   <cfelse>
	      <cfif cgi.http_cookie is "">
	         <cfif StructKeyExists(application.rate_limiter, REQUEST.remoteAddress) and DateDiff("s",application.rate_limiter[REQUEST.remoteAddress].last_attempt,Now()) LT arguments.duration>
	            <cfif application.rate_limiter[REQUEST.remoteAddress].attempts GT arguments.count>
	               <cfoutput><p>You are making too many requests too fast, please slow down and wait #arguments.duration# seconds</p></cfoutput>
	               <cfheader statuscode="503" statustext="Service Unavailable">
	               <cfheader name="Retry-After" value="#arguments.duration#">
	               <cflog file="limiter" text="'limiter invoked for:','#REQUEST.remoteAddress#',#application.rate_limiter[REQUEST.remoteAddress].attempts#,#cgi.request_method#,'#cgi.SCRIPT_NAME#', '#cgi.QUERY_STRING#','#cgi.http_user_agent#','#application.rate_limiter[REQUEST.remoteAddress].last_attempt#',#listlen(cgi.http_cookie,";")#">
	               <cfset application.rate_limiter[REQUEST.remoteAddress].attempts = application.rate_limiter[REQUEST.remoteAddress].attempts + 1>
	               <cfset application.rate_limiter[REQUEST.remoteAddress].last_attempt = Now()>
	               <cfabort>
	            <cfelse>
	               <cfset application.rate_limiter[REQUEST.remoteAddress].attempts = application.rate_limiter[REQUEST.remoteAddress].attempts + 1>
	               <cfset application.rate_limiter[REQUEST.remoteAddress].last_attempt = Now()>
	            </cfif>
	         <cfelse>
	            <cfset application.rate_limiter[REQUEST.remoteAddress] = StructNew()>
	            <cfset application.rate_limiter[REQUEST.remoteAddress].attempts = 1>
	            <cfset application.rate_limiter[REQUEST.remoteAddress].last_attempt = Now()>
	         </cfif>
	      </cfif>
	   </cfif>

	</cffunction>

</cfcomponent>