<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE6">
<meta name="google-site-verification" content="rHtSakGDsnI-jRe2uge0mM03w50dFDYfUmDi-KJI-Nc" />
<meta name="google-site-verification" content="puYHwQda1amP8oI7MIEtPJXu5KGn6X8v3B62qiuH-6g" />
<meta name="google-site-verification" content="rHtSakGDsnI-jRe2uge0mM03w50dFDYfUmDi-KJI-Nc" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="MobileOptimized" content="320" />
<meta name="HandheldFriendly" content="True" />
<meta name="google-site-verification" content="l9367DRtnRmPEL5XbkiPJxUX2HpYHC0Fntl1ZXmsSY8" />

<cfif #structKeyExists(request,"titletag")#>
<cfoutput><title>#request.titletag#</title></cfoutput>
<cfelse>
<title>Horse racing tips with proofed results, betting tools & racing news</title>
</cfif>
<cfif #structKeyExists(request,"nodesc")# >
	<cfif #request.nodesc# eq false >
		<cfif #structKeyExists(request,"descTag")#>
			<cfoutput><meta name="description" content= "#request.descTag#"/></cfoutput>
		<cfelse>
			<meta name="description" content="Horse racing tips from winning punters & Australia's racing media. Validated profit stats, free tips, form guides, betting, greyhounds & harness." />
		</cfif>
	</cfif>
<cfelse>
	<cfif #structKeyExists(request,"descTag")#>
		<cfoutput><meta name="description" content= "#request.descTag#"/></cfoutput>
	</cfif>
</cfif>
<!----cfif #structKeyExists(request,"KeywordsTag")#>
<cfoutput><meta name="keywords" content= "#request.KeywordsTag#"/></cfoutput>
<cfelse>
<meta name="keywords" content= "horse racing tips, tipping competitions, tip comps, betting, horseracing, harness, harnessracing, greyhound racing, greyhoundracing"/>
</cfif---->
<cfif #structKeyExists(request, "nofollow")#>
<meta name="robots" content="noindex, nofollow">
</cfif>
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<cfoutput><META HTTP-EQUIV="EXPIRES" CONTENT="#now()#"></cfoutput>
<cfheader name="pragma" value="no-cache">

<cfoutput>
<link rel="stylesheet" type="text/css" href="#application.stylepath#/BookiePromoStyle.min.css" media="screen" />
<link rel="icon" type="image/gif" href="#request.protocol##application.sitename#/favicon.gif">

<!----link href='http://fonts.googleapis.com/css?family=Ubuntu:regular' rel='stylesheet' type='text/css'---->
<link href='#request.protocol#fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href="#request.protocol#fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type='text/css'>

<!----link rel="stylesheet" type="text/css" href="#application.stylepath#/global_webkit.css?v#dateformat(now(),'yyyymmdd')#" /---->
<!----link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/globalstyle.min.css?v1.1.3" /---->
<link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/globalstyle.min.css?v#timeformat(now(),'hh')#" />
<!----link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/globalstyle-maincss.min.css?v1.1.19" /---->

<link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/globalstyle1200.css?v18#timeformat(now(),'hh')#" />

<link href="#application.stylepath#/toastr.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/responsiveslide.min.css?v1.1.4" />
<!----link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/jquery.bxslider.css?v1.1.1" /---->
<!----link rel="stylesheet" type="text/css" media="all" charset="utf-8" href="#application.stylepath#/keypad-style.css?v1.1.1" /---->
<!----ink rel="stylesheet" type="text/css" media="only screen and (min-device-width: 1200px)" charset="utf-8" href="#application.stylepath#/globalstyle1200.css?v1.1.1" /---->
<link rel="stylesheet" type="text/css" href="#application.jquerypath#/css/numpad-styling.min.css" media="screen" />
<cfif #findnocase('membership', cgi.script_name)#>
	<link rel="stylesheet" type="text/css" href="#application.stylepath#/print.css" media="print" />
</cfif>
<link rel="stylesheet" type="text/css" href="#application.jquerypath#/css/jquery.jscrollpane.min.css" media="screen" />
<link type="text/css" href="#application.jquerypath#/css/tabs.min.css?v1.1" rel="stylesheet" />
<link rel="stylesheet" href="#application.jquerypath#/css/tipsy.min.css" type="text/css" />
<link rel="stylesheet" href="#application.jquerypath#/css/jquery.mobile-1.4.5.min.css?v1.1.3">
<link rel="stylesheet" href="#application.jquerypath#/css/spinner-style.min.css" type="text/css" />

<cfif FindNoCase("tipmarket" ,"#CGI.SCRIPT_NAME#") gt 0 OR FindNoCase("racetrack" ,"#CGI.SCRIPT_NAME#") gt 0 OR FindNoCase("usersite" ,"#CGI.SCRIPT_NAME#") gt 0>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!----script type="text/javascript" src="#application.jquerypath#/js/pagination.js"></script---->
</cfif>

<link rel="stylesheet" href="#application.stylepath#/jquery-ui.css" />

<script type="text/javascript" src="#application.jquerypath#/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<cfif reFindNoCase("(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0>
<script type="text/javascript">$(document).bind("mobileinit", function(){$.extend(  $.mobile , {autoInitializePage: false})});</script>
<script type="text/javascript" src="#application.jquerypath#/jquery.mobile-1.4.5.min.js?v1"></script>
<cfset request.ismobilebrowse = true>
</cfif>

<script type="text/javascript" src="#application.jquerypath#/js/jquery.number.format.js"></script>
<script type="text/javascript" src="#application.jquerypath#/jquery.sparkline.min.js"></script>

<script type="text/javascript" src="#application.funcpath#/jquery/leanModal.js?vs1.1"></script>
<script type="text/javascript" src="#application.funcpath#/gtoscripts/toastr.js"></script>
<script src="#application.jquerypath#/jquery-loader.js" type="text/javascript"></script>
<script type="text/javascript" src="#application.jquerypath#/js/ckeditor_4.5.6/ckeditor.js"></script>
<script type="text/javascript" src="#application.funcpath#/gtoscripts/responsive.min.js?vs1.29#dateformat(now(),'yyyymmdd')#"></script>
<script type="text/javascript" src="#application.funcpath#/gtoscripts/gtocore.min.js?vs1.37#dateformat(now(),'yyyymmdd')#"></script>

<script type="text/javascript" src="#application.funcpath#/gtoscripts/betslip.min.js?vs1.14#timeformat(now(),'hh')#"></script>

<cfif #structKeyExists(url,"home")# eq false AND #findnocase('entrypayment',cgi.QUERY_STRING)# eq false AND #findnocase('affiliate.cfm', cgi.SCRIPT_NAME)# eq false>

		<cfif #structKeyExists(request,"raceday")#>
			<script type="text/javascript" src="#application.funcpath#/gtoscripts/raceday.js?vs1.6#timeformat(now(),'hh')#"></script>
		</cfif>
		<cfif #findnocase("viewRedirectBookieCreate", cgi.script_name)#>
			<script type="text/javascript" src="#application.funcpath#/gtoscripts/bookie-redirect.js"></script>
		</cfif>
		<cfif #findnocase("redirect", cgi.script_name)# AND #findnocase("viewRedirectBookieCreate", cgi.script_name)# eq 0>
			<script type="text/javascript" src="#application.funcpath#/gtoscripts/redirect.js"></script>
		</cfif>
</cfif>
<cfif #findnocase('entrypayment',cgi.QUERY_STRING)#>
	<script type="text/javascript" src="#application.funcpath#/gtoscripts/paymentfunctions.js?v1.1"></script>
</cfif>

<cfif #session.isloggedin# eq true><span id="conf" data-uid="#session.userid#" data-id="#Session.SessionID#" data-site="#application.sitesource#" data-balmin="#application.tipchargemin#" data-wallet="#request.GTOWallet.totalWallet#" data-mybookie="#session.defaultbookie#" <cfif #request.qDefaultBookie.recordcount# gt 0>data-bookiedefault="#request.qDefaultBookie.bookieid#~#request.qDefaultBookie.apiprotocol##request.qDefaultBookie.apipath#"<cfelse>data-bookiedefault="6~"</cfif> ></span><cfelse><span data-uid="#Session.SessionID#" data-id="#Session.SessionID#" data-site="#application.sitesource#" data-bookiedefault="6~"></span></cfif>
</cfoutput>

<!----div style="padding:2px;width:100%;background-color:#83fc32; text-align:center; font-size:1em;clear:both;">
	<p>theGreatTipOff.com is currently unavailable. We are configuring the website upgrade. Please check back later.</p>
</div----->
<cfif structKeyExists(request,"nomenu") eq false>
	<cfinclude template="#application.viewpath#/Shared/toplogin.cfm">
	<cfinclude template="#application.viewpath#/Shared/topbanner.cfm">
<cfelse>
	<cfinclude template="#application.viewpath#/Shared/topbannerblack.cfm">
</cfif>
<!----cfcache action="flush"---->

<cfif #structKeyExists(request,"nomenu")# eq false AND #findnocase('entrypayment',cgi.QUERY_STRING)# eq false>
	<cfinclude template="#application.viewpath#/Shared/navigation.cfm">
</cfif>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?5OyNpleKDKpLn9Camn8faCkyntlzPbPT";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5852447907007583",
    enable_page_level_ads: true
  });
</script>
<!-- Global site tag (gtag.js) - Google Ads: 1024845525 -->
<!---
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1024845525"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1024845525');
</script>
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      //window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-1024845525/ryYBCP-C_YcBENXN1-gD',
      'event_callback': callback
  });
  return false;
}
</script>---->
</head>
<body >
<div id="bubble"><div class="bubblecontent"></div></div>
<div id="bubbleup"><div class="bubblecontent"></div></div>
<div class="mobilebanner" >
	<div style="width:100%;height:35px;margin-top:25px;"></div>
</div>