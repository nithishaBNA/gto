<cfoutput>
<div id="nav">
	<div class="navwrap">
		<cfif #session.afflogin# eq true>
				<cfinclude template="#application.viewpath#/Affiliate/AffiliateMenu.cfm">
		<cfelse>

		<div class="nav_2_of_2">
				<div class="adbox">
					<div id='button-holder'>
					    <cfoutput><img id="searchpunterbutton" alt="Search Punter" src='#application.imagepath#/icon/magnify-30.png' /></cfoutput>
					</div>
					<input type="text" name="sitesearch" value="" id="search-text-input" placeholder="Search Punter or Media Tipster" />
					<cfif #session.isloggedin# eq true>
						<div class="mobilebookielogo">
							<cfif #session.defaultbookie# gt 0>
								<img src="#application.imagepath#/bookmakers/betwith-#session.defaultbookie#-small.png"/>
							</cfif>
						</div>
					</cfif>
					<div id="triggerbetslip" class="pointer">
						<a id="mybetslip" ><img src="#application.imagepath#/icon/betslip-icon.png"><span class="bluecount betslipbutton"><span class="mycount">&nbsp;</span></span></a>
					</div>
					<a name="betslip"></a>
				</div>
				<ul class="betslipcontainer">
					<cfif #session.ipaddressvalid# eq true>
					<li>
						<cfinclude template="#application.viewpath#/shared/betslip.cfm">
					</li>
					</cfif>
				</ul>
		</div>
		<div class="nav_1_of_2">
				<ul class="mainnav">
					<cfif #session.isloggedin# eq true>
							<li class="name"> <a class="myavatar" href=""><img width="25" height="25" class="floatleft" style="margin:-3px 5px 0 0 ;" src="#application.imagepath#/avatar/#session.avatarpath#" onerror="userimgError(this);" /></a> #session.alias#
							<br/><i class="floatleft">Balance</i> &nbsp;<a href="/wallet" id="walletbalance" class="floatleft">&nbsp;<span class="dotline">$ #request.GTOWallet.totalWallet#</span></a>
							</li>

							<li class="status"><a href="/purchase/additem&deposit" class="btn directme">$ Deposit Funds</a></li>
							<cfif #session.defaultbookie# eq 0>
								<li class="status">
									<a class="btn setdefaultbookie" href="/user/bettingaccounts">Start Betting</a>
								</li>
							</cfif>
							<li class="navdivider"></li>
					<cfelse>
							<li class="status">
								<div class="joinstatus">
									<a class="whitefullbtn pointer login_trigger">Login <img class="floatright" alt="Login" src="#application.imagepath#/icon/door-icon.png"/></a>
								</div>
								<div class="joinstatus">
									<a href="/join" class="greenfullbtn pointer">Join Now <img class="floatright" alt="Join" src="#application.imagepath#/icon/pencil-icon.png"/></a>
								</div>
							</li>
					</cfif>
					<li class="status">
						<input type="text" name="sitesearch" value="" id="search-text-input-mobile" placeholder="Search Punter or Media Tipster" />
						<div id='button-holder-mobile'>
						    <cfoutput><img id="searchpunterbutton" alt="Search Punter" src='#application.imagepath#/icon/magnify-30.png' /></cfoutput>
						</div>
					</li>
					<li class="status">
						<div class="quickoptions">
							<a href="/racing" class="greyfullbtn pointer">Racing</a>
						</div>
						<div class="quickoptions">
							<a href="/nexttojump" class="greyfullbtn pointer">Next to Jump</a>
						</div>
						<div class="quickoptions">
							<cfif #session.isloggedin# eq true>
								<a <cfif #request.qDefaultBookie.recordcount# gt 0> target="_blank" href="/redirect?externallink=#request.qDefaultBookie.apiprotocol##request.qDefaultBookie.apipath#/sports"<cfelse>href="/user/bettingaccounts"</cfif> class="greyfullbtn pointer">Sport</a>
							<cfelse>
								<a id="event_s" class="greyfullbtn pointer" >Sport</a>
							</cfif>
						</div>
					</li>

					<!----li><a class="noactive orangelink" href="/racing/flemington/2R25151160-Emirates-Melbourne-Cup"><img alt="Melbourne Cup" src="#application.imagepath#/icon/trophy_icon30.png"/> Melbourne Cup </a> <!---img class="newfeature" src="#application.imagepath#/icon/new-feature.png" /----->
								<ul class="navsuboption" >
									<li class="actionoption"><a href="/racing/flemington/2R25151160-Emirates-Melbourne-Cup">Melbourne Cup Race</a></li>
									<li class="actionoption"><a href="/melbourne-cup-tips">Melbourne Cup Tips</a></li>
									<li class="actionoption spareoption">&nbsp;</li>
								</ul>
						</li---->
					<li <cfif #structKeyExists(url,"home")# eq true>class="selected"</cfif>><a class="noactive " href="#request.protocol##application.sitename#"><img alt="Home" src="#application.imagepath#/icon/icon-home.png"/></a>
						<ul class="navsuboption" >
							<li class="mobileonly tabletoption"><a href="/">Home</a></li>
							<li class="actionoption"><a href="/info/about">About Us</a></li>
							<li class="actionoption"><a href="/info/contact">Contact Us</a></li>
							<li class="actionoption"><a href="/info/help">Help</a></li>
							<li class="actionoption"><a href="/info/sitemap">Sitemap</a></li>
							<!----li class="actionoption"><a href="/melbourne-cup">Melbourne Cup</a></li----->

							<!----li class="actionoption"><a href="/results">Race Results</a></li----->
							<cfif #session.isloggedin# eq true>
								<cfif #session.userid# eq 1 OR #session.userid# eq 3  OR #session.userid# eq 28 OR #session.userid# eq 135 OR #session.userid# eq 151 or #session.roles# eq "Admin">
									<cfif #session.roles# eq "Admin" >
										<li class="actionoption"><a href="/admin/protip/externaltipentry.cfm">Pro Tip Entry</a></li>
									<cfelse>
										<li class="actionoption"><a href="/admin/protip/protipsterentry.cfm">Pro Tip Entry</a></li>
										<li class="actionoption"><a href="/admin/protip/triangulatetips.cfm">Triangulate Tips</a></li>
										<cfif #session.userid# eq 1 ><li class="actionoption"><a href="/admin/protip/Sportsprotipsterentry.cfm">Sports Pro Tip Entry</a></li></cfif>
								      	<li class="actionoption"><a href="/admin">Admin Reporting</a></li>
								      	<li class="actionoption"><a href="/admin/enternews">News Entry</a></li>
								      	<li class="actionoption"><a href="/admin/flexcompconfigure">Configure Flex Comp</a>
											<ul>
									            <li><a href="">PostgreSQL</a></li>
									            <li><a href="">MySql</a></li>
									         </ul>
										</li>
								      	<li class="actionoption"><a href="/admin/recordtipsterpayment">Tipster Payment Report</a></li>
								      	<li class="actionoption"><a href="/admin/PrizeClaimProcess.cfm">Prize Claim Processing</a></li>
								      	<li class="actionoption"><a href="/admin/AffiliateAuthorise.cfm">Authorise Affiliate</a></li>
								      	<li class="actionoption"><a href="/admin/messageuser.cfm">Send Message to User</a></li>
								      	<li class="actionoption"><a href="/admin/email_comp_notify.cfm">Direct Email to User</a></li>
								      </cfif>
								</cfif>
							</cfif>
						</ul>
					</li>
					<!----li class="mainnextjump"><a href="#request.protocol##application.sitename#/nexttojump"> Next to Jump</a></li---->
					<li <cfif #findnocase('tipmarket', cgi.script_name)# gt 0>class="selected"</cfif>><a class="noactive" href="#request.protocol##application.sitename#/tipmarket">Tips</a>

						<ul class="navsuboption" >
							<li class="actionoption"><a href="/tipmarket/horse-racing-tips">Horse Racing Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/randwick-tips">Randwick Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/flemington-tips">Flemington Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/rosehill-tips">Rosehill Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/caulfield-tips">Caulfield Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/moonee-valley-tips">Moonee Valley Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/morphettville-tips">Morphettville Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/doomben-tips">Doomben Tips</a></li>
							<li class="actionoption"><a href="/horse-racing/eagle-farm-tips">Eagle Farm Tips</a></li>
							<li class="actionoption"><a href="/free-horse-racing-tips">Free Horse Racing Tips</a></li>
							<li class="actionoption"><a href="/tipmarket/harness-racing-tips">Harness Racing Tips</a></li>
							<li class="actionoption"><a href="/tipmarket/greyhound-racing-tips">Greyhound Racing Tips</a></li>
							<cfif #session.isloggedin# eq true><li class="actionoption"><a href="/tipping/managetippackage">Manage Tip Packages</a></li></cfif>
						</ul>

					</li>
					<li <cfif #findnocase('leaderboard', cgi.script_name)# gt 0>class="selected"</cfif>><a class="noactive" href="/leaderboard">Leaderboard</a>
						<ul class="navsuboption" >
							<li class="actionoption"><a href="/leaderboard/horseracing">Horse Racing Leaderboard</a></li>
							<li class="actionoption"><a href="/leaderboard/harnessracing">Harness Leaderboard</a></li>
							<li class="actionoption"><a href="/leaderboard/greyhoundracing">Greyhound Leaderboard</a></li>
						</ul>
					</li>
					<li <cfif #findnocase('index.cfm', cgi.script_name)# gt 0 and #request.formguidemode# eq 0 and #structKeyExists(url,"home")# eq false>class="selected"</cfif>><a class="noactive" href="/betting">Bet & Tip</a>
						<ul class="navsuboption" >
							<li class="actionoption"><a href="/racing">Horse Racing</a></li>
							<li class="actionoption"><a href="/harness">Harness Racing</a></li>
							<li class="actionoption"><a href="/greyhound">Greyhound Racing</a></li>
						</ul>
					</li>
					<li  <cfif #request.formguidemode# eq 1>class="selected"</cfif>><a href="/form-guides">Form Guide</a></li>
					<li <cfif #findnocase('competition', cgi.query_string)# gt 0>class="selected"</cfif>><a href="/competitions">Competitions</a></li>
					<!----li><a href="/punters"><img src="#application.imagepath#/icon/icon-punterstipsters.png"/> Punters</a></li---->
					<!----cfif #session.isloggedin# eq true>
								<cfif #request.qDefaultBookie.recordcount# gt 0>
									<li class="mobileonly"><a target="_blank" href="http://#application.sitename#/redirect?externallink=#request.qDefaultBookie.apiprotocol##request.qDefaultBookie.apipath#/sports">Sport</a></li>
								<cfelse>
									<li class="mobileonly"><a href="/user/bettingaccounts">Sport</a></li>
								</cfif>
					<cfelse>
						<li class="mobileonly"><a id="event_s" >Sport</a></li>
					</cfif----->
					<cfif #session.isloggedin# eq true>
							<li><a class="noactive" href="/user/update"> Account </a> <!---img class="newfeature" src="#application.imagepath#/icon/new-feature.png" /----->
								<ul class="navsuboption" >
									<li class="actionoption"><a href="/user/update">Account Details</a></li>
									<li class="actionoption"><a href="/Wallet">Wallet Statement</a></li>
									<li class="actionoption"><a href="/user/bettingaccounts">Betting Accounts</a></li>
									<li class="actionoption"><a href="/sales">Sales</a></li>
									<li class="actionoption"><a href="/pending">Pending (<span class="pendingtrans">0</span>)</a></li>
									<li class="actionoption"><a href="/betresults">Bet Results</a></li>
									<li class="actionoption"><a href="/cancelbet">Cancel a Bet</a></li>
									<li class="actionoption"><a href="/phonebet">Phone Bet</a></li>
									<li class="actionoption"><a href="#application.paymentsite#/Wallet/Cashout" >Wallet Cashout</a></li>
									<li class="actionoption"><a href="/gtosite/sitestyle">Manage Webpage</a><!----br/><img style="float:left;position:absolute;z-index:10;margin:0px 0px 0px 0px;" src="#application.imagepath#/icon/new-feature.png" /----></li>
									<li class="actionoption spareoption">&nbsp;</li>
									<li class="actionoption spareoption">&nbsp;</li>
								</ul>
						</li>
					</cfif>
					<li <cfif #findnocase('news', cgi.query_string)# gt 0>class="selected"</cfif>><a href="/news">News</a></li>

					<li <cfif #findnocase('follow', cgi.script_name)# gt 0>class="selected"</cfif>>
						<cfif #session.isloggedin# eq true>
							<cfquery name="request.qtotalfollow" dbtype = "query">
								SELECT SUM(NFollows) as totalfollows
								FROM request.sFollows.followsummary
							</cfquery>
						</cfif>
						<a <cfif #session.isloggedin# eq true>class="noactive"</cfif> href="/follows">Alerts <cfif #session.isloggedin# eq true><span class="bluecount"><span class="mycount totalfollows"><cfif #request.qtotalfollow.recordcount# eq 0>0<cfelse>#request.qtotalfollow.totalfollows#</cfif></span></span></cfif></a>
						<cfif #session.isloggedin# eq true>
						    <ul class="navsuboption" >
							    <li class="actionoption"><a href="/follows/currentalerts">Upcoming Alerts</a></li>
							    <li class="actionoption"><a href="/follows">Manage Alerts </a><span class="purplecount"><span class="mycount totalfollows"><cfif #request.qtotalfollow.recordcount# eq 0>0<cfelse>#request.qtotalfollow.totalfollows#</cfif></span></span></li>
								<cfloop query="request.sFollows.followsummary">
								<li class="noactionoption" id="follow_cat_#followtypelabel#">#followtypelabel# <span class="bluecount"><span class="mycount" id="follow_cattotal_#followtypelabel#">#NFollows#</span></span></li>
								</cfloop>
							</ul>
						</cfif>
					</li>
					<li <cfif #findnocase('melbourne-cup', cgi.query_string)# gt 0>class="selected"</cfif>><a href="/melbourne-cup">Melbourne Cup</a></li>
					<cfif #session.isloggedin# eq true>
						<li class="mobileonly"><a href="/logout">Logout</a></li>
						<li class="mobileonly">&nbsp;</li>
						<li class="mobileonly">&nbsp;</li>
					</cfif>

				</ul>

		</div>
		</cfif>
	</div>
</div>
</cfoutput>
